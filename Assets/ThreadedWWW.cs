using Common;
using strange.extensions.promise.api;
using strange.extensions.promise.impl;
using System.Collections;
using System.Text;
using System.Threading;
using UnityEngine;

public interface IThreadedWWW
{
	IPromise<string> LoadText(string url);
	IPromise<byte[]> LoadBytes(string url);
}

sealed public class ThreadedWWW : IThreadedWWW
{
	private readonly ICoroutineRunner coroutineRunner;

	public ThreadedWWW(ICoroutineRunner coroutineRunner)
	{
#if UNITY_WEBGL
		throw new System.NotSupportedException("ThreadedWWW is not supported on WebGL.");
#endif

		this.coroutineRunner = coroutineRunner;
	}

	public IPromise<string> LoadText(string url)
	{
		IPromise<string> promise = new Promise<string>();
		coroutineRunner.Start(loadCO(promise, url, WWWFieldProvider.RequestType.Text));
		return promise;
	}

	public IPromise<byte[]> LoadBytes(string url)
	{
		IPromise<byte[]> promise = new Promise<byte[]>();
		coroutineRunner.Start(loadCO(promise, url, WWWFieldProvider.RequestType.Bytes));
		return promise;
	}
	
	private IEnumerator loadCO<T>(IPromise<T> promise, string url, WWWFieldProvider.RequestType request)
	{
		WWW www = new WWW(url);
		object lockObject = new object();

		lock (lockObject)
			yield return www;
		
		WWWFieldProvider provider = new WWWFieldProvider(www, request, lockObject);
		Thread thread = new Thread(provider.Execute);
		thread.Start();

		ThreadState stopState = ThreadState.Aborted | ThreadState.Stopped;
		while ((thread.ThreadState & stopState) == 0)
			yield return null;

		if (typeof(T) == typeof(string))
			((IPromise<string>) promise).Dispatch(provider.Text);
		else
			((IPromise<byte[]>) promise).Dispatch(provider.Bytes);
	}
}

sealed public class WWWFieldProvider
{
	private const int CHARS_PER_STEP = 256;

	public enum RequestType
	{
		Text,
		Bytes
	}

	private readonly WWW www;
	private readonly object lockObject;
	private readonly RequestType request;

	private string text;
	private byte[] bytes;

	public WWWFieldProvider(WWW www, RequestType request, object lockObject)
	{
		this.www = www;
		this.request = request;
		this.lockObject = lockObject;
	}

	public string Text
	{
		get { lock (lockObject) { return text; } }
	}

	public byte[] Bytes
	{
		get { lock (lockObject) { return bytes; } }
	}

	public void Execute()
	{
		if (request == RequestType.Text)
		{
			byte[] raw = null;

			lock (lockObject)
				raw = www.bytes;

			StringBuilder sb = new StringBuilder(raw.Length / 2);
				
			const int bytesPerStep = CHARS_PER_STEP * sizeof(char);

			for (int i = 0; i < raw.Length; i += bytesPerStep)
			{
				int end = System.Math.Min(i + bytesPerStep - 1, raw.Length - 1);
				int toRead = end - i + 1;
				sb.Append(Encoding.UTF8.GetString(raw, i, toRead));
				Thread.Sleep(50);
			}

			lock (lockObject)
				text = sb.ToString();
		}
		else
		{
			lock (lockObject)
				bytes = www.bytes;
		}
	}
}
